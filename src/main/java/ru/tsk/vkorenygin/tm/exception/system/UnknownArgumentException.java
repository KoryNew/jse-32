package ru.tsk.vkorenygin.tm.exception.system;

import ru.tsk.vkorenygin.tm.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException() {
        super("Error! Argument not supported.");
    }

}
