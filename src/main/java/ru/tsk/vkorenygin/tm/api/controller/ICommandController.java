package ru.tsk.vkorenygin.tm.api.controller;

public interface ICommandController {

    void showAbout();

    void showVersion();

    void showInfo();

    void showCommands();

    void showArguments();

    void showHelp();

    void exit();
}
