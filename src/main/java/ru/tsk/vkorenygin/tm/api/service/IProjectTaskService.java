package ru.tsk.vkorenygin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.entity.Task;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;

import java.util.List;

public interface IProjectTaskService {

    @NotNull
    List<Task> findAllTasksByProjectId(@Nullable final String id,
                                       @Nullable final String userId) throws EmptyIdException;

    @NotNull
    Task bindTaskToProject(@Nullable final String projectId,
                           @Nullable final String taskId,
                           @Nullable final String userId) throws AbstractException;

    @NotNull
    Task unbindTaskFromProject(@Nullable final String projectId,
                               @Nullable final String taskId,
                               @Nullable final String userId) throws AbstractException;

}
