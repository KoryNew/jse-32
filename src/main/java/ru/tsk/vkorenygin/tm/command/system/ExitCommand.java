package ru.tsk.vkorenygin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.command.AbstractCommand;

public class ExitCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "exit";
    }

    @Override
    public @Nullable String description() {
        return "close application";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
