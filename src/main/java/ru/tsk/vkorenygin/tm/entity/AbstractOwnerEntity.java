package ru.tsk.vkorenygin.tm.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractOwnerEntity extends AbstractEntity {

    @Nullable
    private String userId = null;

    @Nullable
    public String getUserId() {
        return userId;
    }

    public void setUserId(@Nullable String userId) {
        this.userId = userId;
    }

    @Override
    @NotNull
    public String toString() {
        return super.toString() + "User Id: " + getUserId() + "; ";
    }

}
